﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenT3.Models.Configurations
{
    public class DetalleRutinaConfigurations : IEntityTypeConfiguration<DetalleRutina>
    {
        public void Configure(EntityTypeBuilder<DetalleRutina> builder)
        {
            builder.ToTable("RutinaDetalle");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.Ejercicios).
                WithMany().
                HasForeignKey(o => o.IdEjercicios);
        }
    }
}
