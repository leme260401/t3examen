﻿using ExamenT3.Models.Configurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenT3.Models
{
    public class AppPruebaContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Ejercicios> Ejercicios { get; set; }
        public DbSet<RutinaUsuario> RutinaUsuarios { get; set; }
        public DbSet<DetalleRutina> DetalleRutinas { get; set; }

        public AppPruebaContext(DbContextOptions<AppPruebaContext> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UserConfigurations());
            modelBuilder.ApplyConfiguration(new EjerciciosConfigurations());
            modelBuilder.ApplyConfiguration(new RutinaUsuarioConfigurations());
            modelBuilder.ApplyConfiguration(new DetalleRutinaConfigurations());


        }
    }
}
