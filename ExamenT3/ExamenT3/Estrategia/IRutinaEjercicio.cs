﻿using ExamenT3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenT3.Estrategia
{
    public interface IRutinaEjercicio
    {
        public List<DetalleRutina> Rutina(int idRutina, int ejercicios);
    }
}
