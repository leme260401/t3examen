﻿using ExamenT3.Estrategia;
using ExamenT3.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenT3.Controllers
{
    [Authorize]
    public class EjercicioController : BaseController
    {
        private readonly AppPruebaContext context;
        private readonly IHostEnvironment hostEnv;
        private IRutinaEjercicio tipoRutina;

        public EjercicioController(AppPruebaContext context, IHostEnvironment hostEnv/*, IRutina tipoRutina*/) : base(context)
        {
            this.context = context;
            this.hostEnv = hostEnv;
        }
        [HttpGet]
        public ActionResult Index(int idRutina)
        {
            ViewBag.Ejercicios = context.Ejercicios.ToList();

            var rutina = context.DetalleRutinas.
                Where(o => o.IdRutinaUsuario == idRutina).
                Include(o => o.Ejercicios).
                ToList();

            return View(rutina);
        }

        [HttpGet]
        public ActionResult Rutinas()
        {
            var rutinaUsuario = context.RutinaUsuarios.
                Where(o => o.IdUsuario == LoggedUser().Id).
                ToList();
            return View(rutinaUsuario);
        }

        [HttpGet]
        public ActionResult CrearRutina()
        {
            ViewBag.Tipo = new List<string> { "Principiante", "Intermedio", "Avanzado" };
            return View(new RutinaUsuario());
        }
        [HttpPost]
        public ActionResult CrearRutina(RutinaUsuario rutina)
        {
            rutina.IdUsuario = LoggedUser().Id;
            if (ModelState.IsValid)
            {
                context.RutinaUsuarios.Add(rutina);
                context.SaveChanges();

                int idRutina = rutina.Id;
                var ejercicios = context.Ejercicios.ToList();
                int ejercicio = ejercicios.Count();
                switch (rutina.Tipo)
                {
                    case "Principiante":
                        tipoRutina = new Principiante();
                        break;
                    case "Intermedio":
                        tipoRutina = new Intermedio();
                        break;
                    case "Avanzado":
                        tipoRutina = new Avanzado();
                        break;
                }

                var aplicar = tipoRutina.Rutina(idRutina, ejercicio);

                context.DetalleRutinas.AddRange(aplicar);
                context.SaveChanges();

                return RedirectToAction("Rutinas");
            }
            else
            {
                ViewBag.Tipo = new List<string> { "Principiante", "Intermedio", "Avanzado" };
                return View(new RutinaUsuario());
            }
        }
    }
}
